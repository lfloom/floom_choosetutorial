﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatformController : MonoBehaviour
{
    //variables public and hidden
    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = true;

    //public visible
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck; //linecasting to see if player is standing on the ground

    //private variables
    private bool grounded = false;
    private Animator anim;
    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Awake()
    {
        //constructor 
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        if(Input.GetButtonDown("Jump") && grounded)
        {
            jump = true; 
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h)); //whether player moving left or right, use positive value so speed is always positive

        if(h * rb2d.velocity.x < maxSpeed) //if under speed limit, add force
        {
            rb2d.AddForce(Vector2.right * h * moveForce); 
        }

        if(Mathf.Abs(rb2d.velocity.x) > maxSpeed) //if going too fast, clamp velocity to max speed
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y); //returns 1 if pos num and -1 if neg num
        }

        //flip
        if(h > 0 && !facingRight)
        {
            Flip();
        }
        else if (h < 0 && facingRight)
        {
            Flip(); 
        }

        if(jump)
        {
            anim.SetTrigger("Jump");
            rb2d.AddForce(new Vector2(0f, jumpForce));
            jump = false; //so you can't double jump
        }
    }

    void Flip() //flips sprite around if trying to move that direction (so it faces the direction it's goin)
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1; //inverts it
        transform.localScale = theScale; 
    }
}
